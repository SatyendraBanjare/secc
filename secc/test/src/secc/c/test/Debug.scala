package secc.c.test

import secc.c
import secc.pure.Fun
import secc.pure.Solver

object Debug {
  def main(args: Array[String]) {
//    c.log.level = c.log.Debug

    try {
       Solver.uninterpreted += Fun.mod
      // Solver.timeout = 1000
      val name = "examples/rb-new.c"
      c.verify(name, true)
      Console.out.flush()
      Console.err.flush()
    } catch {
      case e: secc.error.Error =>
        Console.out.flush()
        Console.err.flush()
        println(e.info)
        Console.out.flush()
        Console.err.flush()
    }
  }
}

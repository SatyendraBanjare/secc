package secc.c

package object test {
  val tests = List(
    "examples/test.c",
    "examples/example.c",
    "examples/example-buggy.c",
    "examples/viper.c",
    "examples/array.c",
    "examples/encrypt.c",
    "examples/db.c",
    "examples/string.c",
    "examples/rb.c",
    "examples/cddc.c")
}
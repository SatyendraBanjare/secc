package secc

object SecC {
  var high: Boolean = false

  def configure(args: List[String]): List[String] = args match {
    case Nil => Nil

    case "-low" :: rest =>
      high = false
      configure(rest)

    case "-high" :: rest =>
      high = true
      configure(rest)

    case "-no:div" :: rest =>
      import secc.pure.Fun
      import secc.pure.Solver
      Solver.uninterpreted += Fun.divBy
      configure(rest)

    case "-no:mod" :: rest =>
      import secc.pure.Fun
      import secc.pure.Solver
      Solver.uninterpreted += Fun.mod
      configure(rest)

    case "-timeout" :: s :: rest =>
      import secc.pure.Solver
      Solver.timeout = s.toInt
      configure(rest)

    case "-timeout" :: _ =>
      throw new UnsupportedOperationException("-timeout requires an argument")

    case file :: rest =>
      file :: configure(rest)
  }

  def main(args: Array[String]) {

    if (args.isEmpty) {
      println("usage: secc file1.c file2.c ...")
    } else {
      val files = configure(args.toList)
      for (file <- files) {
        try {
          c.verify(file, high)
        } catch {
          case e: java.io.FileNotFoundException =>
            Console.err.println("  file does not exist")
        }
      }
    }
  }
}
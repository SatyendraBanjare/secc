package secc.c

import secc.error
import secc.heap.Pred
import secc.pure.Fun
import secc.pure.Pure
import secc.pure.Rewrite
import secc.pure.Sort
import secc.pure.Var
import secc.heap.Offset
import secc.pure.App

sealed trait Mode

object Mode {
  case object ghost extends Mode
  case object normal extends Mode
  // case object atomic extends Mode
  // case object readonly extends Mode
}

sealed trait TypedefSort { def name: String }
sealed trait StructSort { def name: String }
sealed trait UnionSort { def name: String }

/**
 * This class represents the context of the symbolic execution that includes definitions and scopes.
 *
 * Note that since all definitions in C can be scoped inside inner functions,
 * type and function definitions are considered to be dynamic.
 */
case class Context(
  /** C type definitions */
  typedefs: Map[String, Type],
  structs: Map[String, Option[List[Field]]],
  unions: Map[String, Option[List[Field]]],
  enums: Map[String, Option[List[String]]],

  /** global and local C variables */
  vars: Map[Id, Type],

  /** numeric constants defined by enums */
  consts: Map[Id, Pure],

  /** C functions */
  funs: Map[Id, (Type, List[Param], Option[Stmt])],

  /** specification annotations of variables and functions */
  specs: Map[Id, List[Spec]],

  /** Types of ghost variables */
  ghost: Map[Id, Type],

  /** logic functions and spatial predicates */
  sig: Map[String, Fun],
  axioms: List[Pure],
  rewrites: List[Rewrite],

  preds: Map[String, Pred],
  defs: Map[String, (List[Id], List[Id], Assert)],

  void: Sort,
  int: Sort,
  char: Sort,
  sec: Sort,

  modes: List[Mode]) {

  import Eval._

  def mode = modes.head

  def defaultState = {
    val st = State.default
    // st assume axioms
    st
  }

  def checkRead(id: Id) = mode match {
    case Mode.ghost =>
      if (!(vars contains id) && !(ghost contains id))
        throw error.InvalidProgram("cannot access: " + id, "undeclared identifier", "current mode: " + mode)
    case Mode.normal =>
      if (!(vars contains id)) {
        if (ghost contains id)
          throw error.VerificationFailure("effects", "cannot access ghost state " + id, "current mode: " + mode)
        else
          throw error.InvalidProgram("cannot access: " + id, "undeclared identifier", "current mode: " + mode)
      }
  }

  def checkMod(id: Id) = mode match {
    case Mode.ghost =>
      if (!(ghost contains id)) {
        if (vars contains id)
          throw error.VerificationFailure("effects", "cannot modify program state " + id, "current mode: " + mode)
        else
          throw error.InvalidProgram("cannot modify: " + id, "undeclared identifier", "current mode: " + mode)
      }
    case Mode.normal =>
      if (!(vars contains id)) {
        if (ghost contains id)
          throw error.VerificationFailure("effects", "cannot modify ghost state " + id, "current mode: " + mode)
        else
          throw error.InvalidProgram("cannot modify: " + id, "undeclared identifier", "current mode: " + mode)
      }
  }

  def checkStore() = mode match {
    case Mode.ghost =>
      throw error.VerificationFailure("effects", "cannot modify the heap", "current mode: " + mode)
    case Mode.normal =>
    // ok to access heap
  }

  def checkCall(expr: FunCall, isLemma: Boolean) = mode match {
    case Mode.ghost if !isLemma =>
      throw error.VerificationFailure("effects", "cannot call non-lemma function", expr, "current mode: " + mode)
    case Mode.ghost =>
    // ok to call lemma function
    case Mode.normal =>
    // ok to call any function
  }

  def enter(mode: Mode) = {
    copy(modes = mode :: modes)
  }

  def leave() = {
    copy(modes = modes.tail)
  }

  def declareGhost(params: List[(Id, Type)]) = {
    copy(ghost = ghost ++ params)
  }

  def declareGhost(id: Id, typ: Type) = {
    copy(ghost = ghost + (id -> typ))
  }

  def declare(id: Id, typ: Type) = mode match {
    case Mode.ghost =>
      copy(ghost = ghost + (id -> typ))
    case Mode.normal =>
      //      if (typ.isLogical)
      //        throw error.VerificationFailure("effects", "logic type for program variable", (id, typ), "current mode: " + mode)
      copy(vars = vars + (id -> typ))
  }

  def declare(params: List[(Id, Type)]) = mode match {
    case Mode.ghost =>
      copy(ghost = ghost ++ params)
    case Mode.normal =>
      //      val lts = params filter (_._2.isLogical)
      //      if (!lts.isEmpty)
      //        throw error.VerificationFailure("effects", "logic type for program variable", lts.mkString(", "), "current mode: " + mode)
      copy(vars = vars ++ params)
  }

  def arbitrary(id: Id, typ: Type): Var = {
    Var.fresh(id.name, resolve(typ))
  }

  def arbitrary(param: Param): (Id, Var) = {
    val Param(typ, name) = param
    val id = Id(name)
    (id, arbitrary(id, typ))
  }

  def arbitrary(id: Id): Var = {
    checkMod(id)

    if (vars contains id)
      return Var.fresh(id.name, resolve(vars(id)))
    if (ghost contains id)
      return Var.fresh(id.name, resolve(ghost(id)))

    throw error.InternalError("unknown checked identifier", id)
  }

  def predicate(name: String, in: List[Param], out: List[Param], body: Option[Assert]) = {
    val _in = in map { case Param(typ, name) => Var(name, resolve(typ)) }
    val _out = out map { case Param(typ, name) => Var(name, resolve(typ)) }
    val pred = Pred(name, _in, _out)
    body match {
      case None =>
        copy(preds = preds + (name -> pred))
      case Some(body) =>
        val xin = in map { case Param(typ, name) => Id(name) }
        val xout = out map { case Param(typ, name) => Id(name) }
        copy(preds = preds + (name -> pred), defs = defs + (name -> (xin, xout, body)))
    }
  }

  def function(name: String, in: List[Param], out: Type, body: Option[Expr]) = {
    val _in = in map { case Param(typ, name) => Var(name, resolve(typ)) }
    val xin = in map { case Param(typ, name) => Id(name) }
    val res = resolve(out)
    val args = _in map (_.typ)
    val fun = Fun(name, args, res)
    body match {
      case None =>
        copy(sig = sig + (name -> fun))
      case Some(body) =>
        val env = Store(xin, _in)
        // Note: no recursive functions
        // - eval will fail because the function is not yet defined
        // - otherwise eval would recurse forever
        val rw = Rewrite(App(fun, _in), eval(body, env, this))
        copy(sig = sig + (name -> fun), rewrites = rw :: rewrites)
    }
  }

  def index(ptr: Pure, index: Pure) = {
    val fun = Offset.index(ptr)
    fun(ptr, index)
  }

  def resolve(fields: List[Field], ptr: Pure, field: String): Pure = fields.find(_.name == field) match {
    case Some(Field(typ, _)) =>
      val res = resolve(typ)
      val fun = Offset.arrow(ptr, field, res)
      fun(ptr)
    case _ =>
      throw error.InvalidProgram("no such field", fields, ptr + "->" + field)
  }

  def resolve(what: String, where: Map[String, Option[List[Field]]], name: String, ptr: Pure, field: String): Pure = {
    if (!(where contains name))
      throw error.InvalidProgram("undeclared " + what, name, ptr + "->" + field)

    val fields = where(name).getOrElse {
      throw error.InvalidProgram("undefined " + what, name, ptr + "->" + field)
    }

    resolve(fields, ptr, field)
  }

  def arrow(ptr: Pure, field: String): Pure = {
    ptr.typ match {
      case Sort.pointer(elem: StructSort) =>
        resolve("struct", structs, elem.name, ptr, field)

      case Sort.pointer(elem: UnionSort) =>
        resolve("union", unions, elem.name, ptr, field)

      case Sort.pointer(elem: TypedefSort) =>
        val name = elem.name
        if (!(typedefs contains name))
          throw error.InvalidProgram("undeclared type", name, ptr + "->" + field)

        typedefs(name) match {
          case AnonStruct(fields) =>
            resolve(fields, ptr, field)
          case AnonUnion(fields) =>
            resolve(fields, ptr, field)
          case _ =>
            throw error.InvalidProgram("not a pointer to struct or union", ptr, ptr + "->" + field)
        }

      case _ =>
        throw error.InvalidProgram("not a pointer to struct or union", ptr, ptr + "->" + field)
    }
  }

  def resolve(typ: Type): Sort = {
    resolve(None, typ)
  }

  def resolve(name: Option[TypeName], typ: Type): Sort = typ match {
    case Void => void
    case SignedInt => int
    case SignedChar => char
    case TypedefName("sec") => sec
    case PtrType(elem) => Sort.pointer(resolve(elem))

    case Type.list(elem) => Sort.list(resolve(elem))
    case Type.array(dom, ran) => Sort.array(resolve(dom), resolve(ran))

    case name: TypedefName =>
      // Note: pick last name in the chain to get foo for typedef struct { ... } foo;
      resolve(Some(name), typedefs(name.name))

    case _: EnumName => Sort.int
    case StructName(name) => new Sort.base(name) with StructSort
    case UnionName(name) => new Sort.base(name) with UnionSort

    case _: CompoundType =>
      name match {
        case None => throw error.InvalidProgram("cannot resolve name of anonymous type", typ)
        case Some(name) => new Sort.base(name.name) with TypedefSort
      }
  }

}

object Context {
  val empty = Context(
    typedefs = Map(),
    structs = Map(),
    unions = Map(),
    enums = Map(),

    vars = Map(),
    consts = Map(),
    funs = Map(),

    specs = Map(),
    ghost = Map(),

    sig = Map(),
    axioms = List(),
    rewrites = List(),

    preds = Map(),
    defs = Map(),

    void = Sort.unit,
    int = Sort.int,
    char = Sort.int,
    sec = Sort.sec,

    modes = Nil)

  val default = empty copy (
    sig = Map(
      "true" -> Fun._true,
      "false" -> Fun._false,
      "low" -> Fun.low,
      "high" -> Fun.high,
      "+" -> Fun.plus,
      "-" -> Fun.minus,
      "*" -> Fun.times,
      "/" -> Fun.divBy,
      "%" -> Fun.mod,
      "<" -> Fun.lt,
      "<=" -> Fun.le,
      ">" -> Fun.gt,
      ">=" -> Fun.ge,
      "!" -> Fun.not,
      "&&" -> Fun.and,
      "||" -> Fun.or,
      "==>" -> Fun.imp,
      "<=>" -> Fun.eqv,

      "nil" -> Fun.nil,
      "cons" -> Fun.cons,
      "in" -> Fun.in,
      "head" -> Fun.head,
      "tail" -> Fun.tail,
      "last" -> Fun.last,
      "init" -> Fun.init,

      "select" -> Fun.select,
      "store" -> Fun.store),

    axioms = secc.pure.axioms,

    modes = List(Mode.normal))
}
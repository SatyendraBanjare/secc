package secc.c

import secc.pure.Pure
import secc.pure.Const
import secc.error
import secc.pure.Sort
import secc.pure.Fun
import secc.pure.Rewrite
import secc.pure.Var
import secc.pure.Ex
import secc.pure.All
import secc.pure.Simplify

object Eval {
  import Prove.prove
  import Prove.consume
  import Prove.produce

  def rvals(exprs: List[Expr], st0: State, ctx: Context): List[(List[Pure], State)] = exprs match {
    case Nil =>
      List((Nil, st0))

    case expr :: rest => // XXX: right-to-left, should be parallel
      for (
        (xs, st1) <- rvals(rest, st0, ctx);
        (x, st2) <- rval(expr, st1, ctx)
      ) yield (x :: xs, st2)
  }

  def truth(arg: Pure): Pure = arg.typ match {
    case Sort.bool => arg
    case Sort.int => arg !== 0
    case _ => throw error.InvalidProgram("not boolean", arg)
  }

  def app(fun: Fun, args: Pure*) = {
    val _args = for ((typ, arg) <- fun.args zip args) yield typ match {
      case Sort.bool => truth(arg)
      case _ => arg
    }
    fun(_args: _*)
  }

  def add(arg1: Pure, arg2: Pure, ctx: Context) = (arg1.typ, arg2.typ) match {
    case (Sort.pointer(elem), ctx.int) => ctx index (arg1, arg2)
    case (ctx.int, ctx.int) => arg1 + arg2
    case _ => throw error.InvalidProgram("invalid arithmetic operation", arg1 + " + " + arg2, arg1.typ, arg2.typ)
  }

  def sub(arg1: Pure, arg2: Pure, ctx: Context) = (arg1.typ, arg2.typ) match {
    case (Sort.pointer(elem), ctx.int) => ctx index (arg1, -arg2)
    case (ctx.int, ctx.int) => arg1 - arg2
    case _ => throw error.InvalidProgram("invalid arithmetic operation", arg1 + " - " + arg2, arg1.typ, arg2.typ)
  }

  def rval_low_test(expr: Expr, st0: State, ctx: Context): List[(Pure, State)] = {
    for ((_res, st1) <- rval(expr, st0, ctx)) yield {
      val _test = truth(_res)
      prove(_test :: st1.attacker, st1, ctx)
      (_test, st1)
    }
  }

  def asg(lhs: Expr, rhs: Expr, st0: State, ctx: Context): List[(Pure, Pure, State)] = lhs match {
    case id: Id if (st0.store contains id) =>
      ctx.checkMod(id)
      val _old = st0.store(id)
      for (
        (_rhs, st1) <- rval(rhs, st0, ctx)
      ) yield {
        (_old, _rhs, st1 assign (id, _rhs))
      }

    case PreOp("*", ptr) =>
      ctx.checkStore()
      for (
        (_rhs, st1) <- rval(rhs, st0, ctx);
        (_ptr, st2) <- rval(ptr, st1, ctx)
      ) yield {
        val (_sec, _old, st3) = st2 store (_ptr, _rhs)
        prove(_ptr :: _sec, st3, ctx)
        prove(_rhs :: _sec, st3, ctx)
        (_old, _rhs, st3)
      }

    case Arrow(ptr, field) =>
      ctx.checkStore()
      for (
        (_rhs, st1) <- rval(rhs, st0, ctx);
        (_ptr, st2) <- rval(ptr, st1, ctx)
      ) yield {
        val _ptr_field = ctx arrow (_ptr, field)
        val (_sec, _old, st3) = st2 store (_ptr_field, _rhs)
        prove(_ptr_field :: _sec, st3, ctx)
        prove(_rhs :: _sec, st3, ctx)
        (_old, _rhs, st3)
      }
  }

  // https://en.cppreference.com/w/cpp/language/eval_order
  def rval(expr: Expr, st0: State, ctx: Context): List[(Pure, State)] = expr match {
    case BinOp(",", fst, snd) =>
      for (
        (_fst, st1) <- rval(fst, st0, ctx);
        (_snd, st2) <- rval(fst, st1, ctx)
      ) yield (_snd, st2)

    case id: Id if (st0.store contains id) =>
      ctx.checkRead(id)
      List((st0 store id, st0))

    case id: Id if (ctx.consts contains id) =>
      List((ctx consts id, st0))

    case id: Id if ctx.mode == Mode.ghost && (ctx.sig contains id.name) =>
      val _fun = ctx sig id.name
      List((_fun(), st0))

    case id: Id =>
      throw error.InvalidProgram("invalid identifier", expr, st0)

    case Lit(arg: Int) =>
      List((Const(arg.toString, Sort.int), st0))

    case PreOp("&", id: Id) =>
      throw error.InvalidProgram("cannot take address of variable", expr, st0)

    case PreOp("&", PreOp("*", ptr)) =>
      rval(ptr, st0, ctx)
    // for ((_ptr, st1) <- rval(ptr, st0, ctx)) yield {
    //   (Ref(Loc.at(_ptr)), st1)
    // }

    case PreOp("&", Arrow(ptr, field)) =>
      for ((_ptr, st1) <- rval(ptr, st0, ctx)) yield {
        val _ptr_field = ctx arrow (_ptr, field)
        (_ptr_field, st1)
      }

    case PreOp("*", ptr) =>
      for ((_ptr, st1) <- rval(ptr, st0, ctx)) yield {
        val (_sec, _res) = st1 load _ptr
        (_res, st1)
      }

    case Arrow(ptr, field) =>
      for ((_ptr, st1) <- rval(ptr, st0, ctx)) yield {
        val _ptr_field = ctx arrow (_ptr, field)
        val (_sec, _res) = st1 load _ptr_field
        (_res, st1)
      }

    case PreOp("++", arg) =>
      for ((_, _rhs, st1) <- asg(arg, BinOp("+", arg, Lit(1)), st0, ctx))
        yield (_rhs, st1)
    case PreOp("--", arg) =>
      for ((_, _rhs, st1) <- asg(arg, BinOp("-", arg, Lit(1)), st0, ctx))
        yield (_rhs, st1)

    case PostOp("++", arg) =>
      for ((_val, _, st1) <- asg(arg, BinOp("+", arg, Lit(1)), st0, ctx))
        yield (_val, st1)
    case PostOp("--", arg) =>
      for ((_val, _, st1) <- asg(arg, BinOp("-", arg, Lit(1)), st0, ctx))
        yield (_val, st1)

    case BinOp("=", lhs, rhs) =>
      for ((_, _rhs, st1) <- asg(lhs, rhs, st0, ctx))
        yield (_rhs, st1)

    case BinOp("==", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx)) yield {

        if (_arg1.typ != _arg2.typ) {
          println(expr)
        }
        (_arg1 === _arg2, st1)
      }

    case BinOp("!=", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 !== _arg2, st1)

    case PreOp("!", arg) =>
      for ((_arg, st1) <- rval(arg, st0, ctx))
        yield (!truth(_arg), st1)

    case BinOp("+", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (add(_arg1, _arg2, ctx), st1)

    case BinOp("-", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (sub(_arg1, _arg2, ctx), st1)

    case BinOp("*", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 * _arg2, st1)

    case BinOp("/", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 / _arg2, st1)

    case BinOp("%", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 % _arg2, st1)

    case BinOp("<=", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 <= _arg2, st1)

    case BinOp("<", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 < _arg2, st1)

    case BinOp(">=", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 >= _arg2, st1)

    case BinOp(">", arg1, arg2) =>
      for ((List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (_arg1 > _arg2, st1)

    // don't fork if the rhs has no side effects
    case BinOp("||", arg1, arg2) if !Syntax.hasEffects(arg2) =>
      for (
        (_arg1, st1) <- rval_low_test(arg1, st0, ctx);
        (_arg2, st2) <- rval(arg2, st1, ctx)
      ) yield (truth(_arg1) || truth(_arg2), st2)

    case BinOp("&&", arg1, arg2) if !Syntax.hasEffects(arg2) =>
      for (
        (_arg1, st1) <- rval_low_test(arg1, st0, ctx);
        (_arg2, st2) <- rval(arg2, st1, ctx)
      ) yield (truth(_arg1) && truth(_arg2), st2)

    // shortcut evaluation yields two states
    case BinOp("||", arg1, arg2) =>
      val _arg1_st = rval_low_test(arg1, st0, ctx)

      val _true = for (
        (_arg1, st1) <- _arg1_st;
        st1_true <- st1 && truth(_arg1)
      ) yield (secc.pure.True, st1_true)

      val _false = for (
        (_arg1, st1) <- _arg1_st;
        st1_false <- st1 && !truth(_arg1);
        (_arg2, st2) <- rval(arg2, st1_false, ctx)
      ) yield (_arg2, st2)

      _true ++ _false

    // shortcut evaluation yields two states
    case BinOp("&&", arg1, arg2) =>
      val _arg1_st = rval_low_test(arg1, st0, ctx)

      val _false = for (
        (_arg1, st1) <- _arg1_st;
        st1_false <- st1 && !truth(_arg1)
      ) yield (secc.pure.False, st1_false)

      val _true = for (
        (_arg1, st1) <- _arg1_st;
        st1_true <- st1 && truth(_arg1);
        (_arg2, st2) <- rval(arg2, st1_true, ctx)
      ) yield (_arg2, st2)

      _false ++ _true

    case Question(test, left, right) if !Syntax.hasEffects(left) && !Syntax.hasEffects(right) =>
      for (
        (_test, st1) <- rval_low_test(test, st0, ctx);
        (_left, st2) <- rval(left, st1, ctx);
        (_right, st3) <- rval(right, st2, ctx)
      ) yield (truth(_test) ? (_left, _right), st1)

    case Question(test, left, right) =>
      val _test_st = rval_low_test(test, st0, ctx)

      val _true = for (
        (_test, st1) <- _test_st;
        st1_true <- st1 && truth(_test);
        (_left, st2) <- rval(left, st1_true, ctx)
      ) yield (_left, st2)

      val _false = for (
        (_test, st1) <- _test_st;
        st1_false <- st1 && !truth(_test);
        (_right, st2) <- rval(right, st1_false, ctx)
      ) yield (_right, st2)

      _true ++ _false

    case expr @ FunCall(id, args) =>
      if (!(ctx.specs contains id))
        throw error.InvalidProgram("no specification", expr)

      val specs = ctx specs id
      val Prepost(pres, posts, fails) = Prepost(specs)
      val pre = And(pres)
      val post = And(posts)
      val isLemma = specs contains Lemma

      for (fail <- fails) {
        throw error.VerificationFailure(fail, "failure caused by propagating fails annotation on callee", id)
      }

      ctx.checkCall(expr, isLemma)

      val (typ, params, _) = ctx funs id
      val xr = ctx arbitrary (Id.result, typ)
      val ids = params map { case Param(typ, name) => Id(name) }

      for (
        (_args, st1) <- rvals(args, st0, ctx);
        (env, st2) <- consume(pre, st1.store ++ Store(Id.result :: ids, xr :: _args), st1, ctx);
        (st3, _) <- produce(post, env, st2, ctx, bind = false) // Note: ctx is unchanged for bind = false
      ) yield {
        (xr, st3)
      }
  }

  def eval_low_test(expr: Expr, env: Store, st: State, ctx: Context): Pure = {
    val _test = eval_test(expr, env, ctx)
    prove(_test :: st.attacker, st, ctx)
    _test
  }

  def eval_test(expr: Expr, st: Store, ctx: Context) = {
    truth(eval(expr, st, ctx))
  }

  def axiom(expr: Expr, st: Store, ctx: Context): Pure = {
    val pure = eval(expr, st, ctx)
    if (!pure.free.isEmpty)
      throw error.InvalidProgram("invalid axiom", "free variables: " + pure.free)
    // ensure that function definitions are applied
    Simplify.simplify(pure, ctx.rewrites)
  }

  def rewrite(expr: Expr, st: Store, ctx: Context): Rewrite = eval(expr, st, ctx) match {
    case pure if !pure.free.isEmpty =>
      throw error.InvalidProgram("invalid rewrite", "free variables: " + pure.free)
    case Pure._eq(lhs, rhs) =>
      Rewrite(lhs, rhs)
    case Pure.imp(pre, Pure._eq(lhs, rhs)) =>
      Rewrite(lhs, rhs, Some(pre))
    case All(bound, Pure._eq(lhs, rhs)) =>
      Rewrite(lhs, rhs)
    case All(bound, Pure.imp(pre, Pure._eq(lhs, rhs))) =>
      Rewrite(lhs, rhs, Some(pre))
    case _ =>
      throw error.InvalidProgram("invalid rewrite", "not one of", "lhs == rhs", "pre ==> lhs == rhs")
  }

  def eval(expr: Expr, st: Store, ctx: Context): Pure = expr match {
    case id: Id if st contains id =>
      st(id)

    case id: Id if ctx.consts contains id =>
      ctx consts id

    case id: Id if ctx.sig contains id.name =>
      val _fun = ctx sig id.name
      _fun()

    case id: Id =>
      throw error.InvalidProgram("invalid identifier", expr, st)

    case Lit(arg: Int) =>
      Const(arg.toString, Sort.int)

    case PreOp("&", PreOp("*", ptr)) =>
      eval(ptr, st, ctx)
    // val _ptr = eval(ptr, st, ctx)
    // Ref(Loc.at(_ptr))

    case PreOp("&", Arrow(ptr, field)) =>
      val _ptr = eval(ptr, st, ctx)
      ctx arrow (_ptr, field)

    case PreOp(op, arg) if (ctx.sig contains op) =>
      val _fun = ctx sig op
      val _arg = eval(arg, st, ctx)
      app(_fun, _arg)

    // polymorphic operators are treated explicitly
    case BinOp("==", arg1, arg2) =>
      val _arg1 = eval(arg1, st, ctx)
      val _arg2 = eval(arg2, st, ctx)
      _arg1 === _arg2

    case BinOp("!=", arg1, arg2) =>
      val _arg1 = eval(arg1, st, ctx)
      val _arg2 = eval(arg2, st, ctx)
      _arg1 !== _arg2

    case BinOp("::", arg1, arg2) =>
      val _arg1 = eval(arg1, st, ctx)
      val _arg2 = eval(arg2, st, ctx)
      _arg1 :: _arg2

    // addition/substraction are overloaded for integers/pointer arithmetic
    case BinOp("+", arg1, arg2) =>
      val _arg1 = eval(arg1, st, ctx)
      val _arg2 = eval(arg2, st, ctx)
      add(_arg1, _arg2, ctx)

    case BinOp("-", arg1, arg2) =>
      val _arg1 = eval(arg1, st, ctx)
      val _arg2 = eval(arg2, st, ctx)
      sub(_arg1, _arg2, ctx)

    case BinOp(op, arg1, arg2) if ctx.sig contains op =>
      val _fun = ctx sig op
      val _arg1 = eval(arg1, st, ctx)
      val _arg2 = eval(arg2, st, ctx)
      app(_fun, _arg1, _arg2)

    case BinOp(op, arg1, arg2) =>
      throw error.InvalidProgram("undefined operator", op, ctx.sig)

    case FunCall(id, args) if ctx.sig contains id.name =>
      val _fun = ctx sig id.name
      val _args = args map (eval(_, st, ctx))
      app(_fun, _args: _*)

    case FunCall(id, args) =>
      throw error.InvalidProgram("undefined function", id, ctx.sig)

    case Question(test, left, right) =>
      val _test = eval_test(test, st, ctx)
      val _left = eval(left, st, ctx)
      val _right = eval(right, st, ctx)
      _test ? (_left, _right)

    case Bind(how, params, body) =>
      val binding = params map {
        case Param(typ, name) =>
          val id = Id(name)
          val sort = ctx resolve typ
          val x = Var(name, sort)
          (id -> x, x)
      }

      val (env, bound) = binding.unzip
      val _st = st ++ env

      how match {
        case "exists" => Ex(bound.toSet, eval(body, _st, ctx))
        case "forall" => All(bound.toSet, eval(body, _st, ctx))
      }
  }

  def open(pred: String, in: List[Expr], out: List[Expr], st: State, ctx: Context) = {
    val _pred = ctx preds pred
    val (xin, xout, body) = ctx defs pred
    val _in = in map (eval(_, st.store, ctx))
    val _out = out map (eval(_, st.store, ctx))
    val env = Store(xin ++ xout, _in ++ _out)
    (_pred, _in, _out, body, env)
  }
}
package secc

import java.io.File
import java.io.FileReader
import java.io.InputStreamReader
import java.io.Reader

import scala.collection.JavaConverters.asScalaBufferConverter

package object c {
  import secc.pure.Pure
  type Store = Map[Id, Pure]

  object Store {
    def apply(ids: Iterable[Id], args: Iterable[Pure]) = {
      val st = (ids zip args)
      st.toMap
    }
  }

  val High = Id("high")
  val Low = Id("low")
  val False = Lit(0)
  val True = Lit(1)

  def verify(path: String, high: Boolean) {
    val stmts = parse(path)
    Verify.file(path, high: Boolean, stmts)
  }

  def parse(): List[Global] = {
    parse(new InputStreamReader(System.in), "-")
  }

  def parse(path: String): List[Global] = {
    parse(new FileReader(path), path)
  }

  def parse(file: File): List[Global] = {
    parse(new FileReader(file), file.getPath)
  }

  def parse(reader: Reader, path: String): List[Global] = {
    val scanner = new Scanner(reader)
    val parser = new Parser()

    val types = new java.util.HashSet[String]
    val preds = new java.util.HashSet[String]

    types.add("sec")

    scanner.types = types
    scanner.preds = preds

    parser.types = types
    parser.preds = preds

    val result = try {
      parser parse scanner
    } catch {
      case e: Exception =>
        throw error.InvalidProgram("parse", path, e)
    }

    if (result != null) {
      val globals: List[Global] = result.asInstanceOf[java.util.ArrayList[Global]].asScala.toList
      globals
    } else {
      Nil
    }
  }
}
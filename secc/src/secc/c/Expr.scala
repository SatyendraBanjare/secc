package secc.c

sealed trait Expr extends Assert {
}

case class Field(typ: Type, name: String) extends beaver.Symbol {
  override def toString = typ + " " + name
}

case class Param(typ: Type, name: String) extends beaver.Symbol {
  override def toString = typ + " " + name
}

case class Lit(arg: Any) extends Expr {
  override def toString = arg.toString
}

case class Id(name: String) extends Expr {
  override def toString = name
}

object Id {
  val result = Id("result")
  val main = Id("main")
}

case class PreOp(op: String, arg: Expr) extends Expr {
  override def toString = "(" + op + " " + arg + ")"
}

case class PostOp(op: String, arg: Expr) extends Expr {
  override def toString = "(" + arg + " " + op + ")"
}

case class BinOp(op: String, arg1: Expr, arg2: Expr) extends Expr {
  override def toString = "(" + arg1 + " " + op + " " + arg2 + ")"
}

case class Question(test: Expr, left: Expr, right: Expr) extends Expr {
  override def toString = "(" + test + " ? " + left + " : " + right + ")"
}

case class SizeOfType(typ: Type) extends Expr {
  def free = Set()
  override def toString = "sizeof(" + typ + ")"
}

case class SizeOfExpr(expr: Expr) extends Expr {
  override def toString = "sizeof(" + expr + ")"
}

case class Cast(typ: Type, expr: Expr) extends Expr {
  override def toString = "(" + typ + ")" + expr
}

case class Arrow(expr: Expr, field: String) extends Expr {
  override def toString = expr + "->" + field
}

case class FunCall(fun: Id, args: List[Expr]) extends Expr { // no function pointers
  def this(name: String, args: Array[Expr]) = this(Id(name), args.toList)
  override def toString = fun + args.mkString("(", ", ", ")")
}

case class Init(values: List[(Option[String], Expr)]) extends Expr { // { .field = value } or { value }
}

case class Bind(how: String, params: List[Param], body: Expr) extends Expr {
  def this(how: String, params: Array[Param], body: Expr) = this(how, params.toList, body)
  
  override def toString = {
    how + params.mkString(" ", ", ", ". ") + body
  }
}


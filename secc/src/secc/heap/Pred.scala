package secc.heap

import secc.pure.Pure

case class Pred(name: String, in: List[Pure], out: List[Pure]) {
  override def toString = name
  def apply(in: Pure*)(out: Pure*) = Chunk(this, in.toList, out.toList)
}
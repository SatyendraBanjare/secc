.PHONY: all test clean parser check-dependencies macos_sip_fix

MILL = ./mill

SECC_EXAMPLES = examples

VPATH     = $(SECC_EXAMPLES)
SECC_C    = $(wildcard $(SECC_EXAMPLES)/*.c)
SECC_OBJ  = $(SECC_C:$(SECC_EXAMPLES)/%.c=obj/%.o)
SECC_JAVA = secc/src/secc/c/Parser.java \
            secc/src/secc/c/Scanner.java

CC ?= cc
CFLAGS ?= -Iinclude -Wall -W -Wpedantic

SECC_JAR = out/secc/jar/dest/out.jar
SECC_LAUNCHER = ./out/secc/launcher/dest/run
SECC_SH  = ./SecC.sh

all: $(SECC_JAR) $(SECC_SH) macos_sip_fix

parser: $(SECC_JAVA)

test:
	$(MILL) secc.test

clean:
	$(MILL) clean
	rm -f $(SECC_SH)

check-dependencies:
	$(MILL) mill.scalalib.Dependency/updates

$(SECC_LAUNCHER):
	@echo $@
	$(MILL) secc.launcher

$(SECC_JAR):
	@echo $@
	$(MILL) secc.jar

$(SECC_SH): $(SECC_LAUNCHER)
	@echo "[echo]  $@"; echo "#!/usr/bin/env bash" > $@; echo "export LD_LIBRARY_PATH=$(PWD)/secc/lib" >> $@; echo "source $(SECC_LAUNCHER)" >> $@
	@echo "[chmod] $@"; chmod +x $@

%.java: %.grammar
	beaver -t $^

%.java: %.flex
	jflex -nobak $^

o: $(SECC_OBJ)
	@echo $(SECC_OBJ)

macos_sip_fix: secc/lib/libz3java.dylib secc/lib/libz3.dylib
	@if [ $$(uname -s) = "Darwin" ];  then \
	    make -s libz3java.dylib libz3.dylib; \
	 fi

lib%.dylib: secc/lib/lib%.dylib
	ln -s $<

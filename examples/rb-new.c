_(rewrites
    forall int a, int b.
        0 <= a && 0 < b && a <= b ==> a%b == a;
	forall int a, int b.
		0 <= a && 0 < b ==> (a + b)%b == a%b)

_(predicate ar(int *a; int n)
    0 < n ==> exists int x.
        a |-> x && ar(a+1; n-1))

/* Slice a[i%n]...a[j%n] of array a[0]...a[n] */
_(predicate slice(int *a, int i, int j; int n, sec l)
	i < j ==> exists int x.
		&a[i%n] |-> x && x :: l && slice(a, i+1, j; n, l))

void slice_0(int *a, int i, int n, sec l)
    _(lemma)
    _(ensures slice(a, i, i; n, l))
{
    _(fold slice(a, i, i; n, l))
}

void slice_1(int *a, int i, int n, sec l)
    _(lemma)
    _(requires exists int x. &a[i%n] |-> x && x :: l)
    _(ensures slice(a, i, i+1; n, l))
{
    slice_0(a, i+1, n, l);
	_(fold slice(a, i, i+1; n, l))
}

void slice_snoc(int *a, int i, int j, int n, sec l)
    _(lemma)
    _(requires i <= j && i :: low && j :: low)
    _(requires slice(a, i, j; n, l))
    _(requires exists int x. &a[j%n] |-> x && x :: l)
    _(ensures  slice(a, i, j+1; n, l))
{
    _(unfold slice(a, i, j; n, l))
    if(i < j) {
        slice_snoc(a, i+1, j, n, l);
        _(fold slice(a, i, j+1; n, l))
    } else {
        slice_1(a, j, n, l);
    }
}

void slice_concat(int *a, int i, int k, int j, int n, sec l)
    _(lemma)
    _(requires k <= j)
    _(requires i :: low && k :: low && j :: low)
    _(requires slice(a, i, k; n, l))
    _(requires slice(a, k, j; n, l))
    _(ensures  slice(a, i, j; n, l))
{
    _(unfold slice(a, i, k; n, l))
    if(i < k) slice_concat(a, i+1, k, j, n, l);
    _(fold slice(a, i, j; n, l))
}

void slice_high(int *a, int i, int j, int n, sec l)
    _(lemma)
    _(requires i :: low && j :: low)
    _(requires slice(a, i, j; n, l))
    _(ensures  slice(a, i, j; n, high))
{
    _(unfold slice(a, i, j; n, l))
    if(i < j) slice_high(a, i+1, j, n, l);
    _(fold slice(a, i, j; n, high))
}

void slice_rotate(int *a, int i, int j, int n, sec l)
    _(lemma)
    _(requires i :: low && j :: low && n :: low && 0 <= i && i <= j && 0 < n)
    _(requires slice(a, i, i+n; n, l))
    _(ensures  slice(a, j, j+n; n, l))
{
   if(i < j) {
       _(unfold slice(a, i, i+n; n, l))
       _(assert (i+n)%n == i%n)
       slice_snoc(a, i+1, i+n, n, l);
       slice_rotate(a, i+1, j, n, l);
   }
}

void slice_from_ar(int *a, int i, int k, int n)
    _(lemma)
    _(requires ar(a + i; k))
    _(requires k :: low && 0 <= i && 0 <= k && i + k <= n)
    _(ensures  slice(a, i, i + k; n, high))
{
    _(unfold ar(a + i; k))
    if(k != 0) {
        _(assert 0 <= i && 0 < n && i <= n) // triggers the rewrite rule
        _(assert i%n == i)
        slice_from_ar(a, i+1, k-1, n);
    }
    _(fold slice(a, i, i + k; n, high))
}

typedef struct {
    int *data;        /* the backing memory */
    int capacity;     /* Previously: Must be a power of two, now scrapped, using modulo */
    int write_index;  /* intentionally not wrapped to capacity */
} rb_t;

_(predicate rb(rb_t *buf; int *_data, int _capacity, int _write_index, int _read_index, sec _level)
    &buf->data        |-> _data &&
    &buf->capacity    |-> _capacity &&
    &buf->write_index |-> _write_index &&
   _capacity :: low && _write_index :: low && _read_index :: low &&
    0 <= _read_index && _read_index <= _write_index && 0 < _capacity &&
    slice(_data, _read_index, _write_index; _capacity, _level) &&
    slice(_data, _write_index, _read_index + _capacity; _capacity, high))


int rb_init(rb_t *buf, int capacity, int *backing)
    _(requires exists int * _data, int _capacity, int _write_index.
        &buf->data        |-> _data &&
        &buf->capacity    |-> _capacity &&
        &buf->write_index |-> _write_index)
    _(requires 0 < capacity && capacity :: low)
    _(requires ar(backing; capacity))
    _(ensures  rb(buf; backing, capacity, 0, 0, low))
{
    buf->data = backing;
    buf->capacity = capacity;
    buf->write_index = 0;
    _(fold slice(backing, 0, 0; capacity, low))
    _(slice_from_ar(backing, 0, capacity, capacity);)
    _(fold rb(buf; backing, capacity, 0, 0, low))
    return 0;
}

void rb_put(rb_t * buf, int byte)
    _(requires exists int * _data, int _c, int _w, int _r, sec _l.
        rb(buf; _data, _c, _w, _r, _l))
    _(requires byte :: _l)
    _(ensures  rb(buf; _data, _c, _w + 1, _r, _l))
{
    _(assume _w < _r + _c) // XXX
    _(unfold rb(buf; _data, _c, _w, _r, _l))
    _(unfold slice(_data, _w, _r + _c; _c, high))
    
    int index = buf->write_index % buf->capacity;

    buf->data[index] = byte;
    buf->write_index++;
    
    _(apply slice_snoc(_data, _r, _w, _c, _l);)
    _(fold rb(buf; _data, _c, _w + 1, _r, _l))
}

int rb_get(rb_t *buf, int *read_index, int *out)
    _(requires exists int *_data, int _c, int _w, int _r, sec _l.
        rb(buf; _data, _c, _w, _r, _l))
    _(requires read_index |-> _r) 
    _(requires exists int _old_out. out |-> _old_out)
    _(requires _w <= _r + _c) // XXX

    _(ensures exists int _new_r.
        rb(buf; _data, _c, _w, _new_r, _l))
    _(ensures read_index |-> _new_r) 
    _(ensures _new_r == (result == 0 ? _r + 1 : _r))
    _(ensures result == 0
        ? exists int _out. out |-> _out && _out :: _l
        : out |-> _old_out)
{
    _(unfold rb(buf; _data, _c, _w, _r, _l))
    if ((*read_index) % buf->capacity != buf->write_index % buf->capacity) {
    	_(unfold slice(_data, _r, _w; _c, _l))
    	
        int index = (*read_index) % buf->capacity;
        (*out) = ((buf->data)[index]);
        *read_index = *read_index + 1;
    	
        _(assert (_r + _c)%_c == _r%_c)
    	_(apply slice_snoc(_data, _w, _r+_c, _c, high);)
        _(fold rb(buf; _data, _c, _w, _r+1, _l))
        return 0;
    } else {
        _(fold rb(buf; _data, _c, _w, _r, _l))
        return 1;
    }
}

void rb_clear(rb_t * buf, int *read_index)
    _(requires exists int * _data, int _c, int _w, int _r, sec _l.
        rb(buf; _data, _c, _w, _r, _l))
    _(requires read_index |-> _r)
    _(requires _w <= _r + _c) // XXX

    _(ensures rb(buf; _data, _c, _w, _w, low))
    _(ensures read_index |-> _w)
{
    _(unfold rb(buf; _data, _c, _w, _r, _l))
    *read_index = buf->write_index;
    _(slice_0(_data, _w, _c, low);)
    _(slice_high(_data, _r, _w, _c, _l);)
    _(slice_concat(_data, _r, _w, _r + _c, _c, high);)
    _(slice_rotate(_data, _r, _w, _c, high);)
    _(fold rb(buf; _data, _c, _w, _w, low))
}
  

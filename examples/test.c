#include "secc.h"

int zero()
	_(ensures result == 0)
	_(ensures result :: low())
{
	return 0;
}

int deref(int *p)
	_(requires exists int v. p |-> v)
	_(ensures p |-> v)
	_(ensures result == v)
{
	return *p;
}

void disjoint(int *p, int *q)
	_(maintains exists int v, int w. p |-> v && q |-> w)
	_(ensures p != q)
{
  // we need to assert the disjointness explicitly
  _(assert p != q)
}

struct pair {
	int fst;
	int snd;
};

void disjoint_pair(struct pair *p)
	_(maintains exists int v, int w. &p->fst |-> v && &p->snd |-> w)
	_(ensures &p->fst != &p->snd)
{
  _(assert &p->fst != &p->snd)
}

void disjoint_pair_incorrect(struct pair *p)
  _(ensures &p->fst != &p->snd)
  _(fails incorrect)
{
  // there is no precondition from which to derive disjointness
  _(assert &p->fst != &p->snd)
}

int aux()
	_(fails memory)
	_(ensures result == 0)
{
	int *p;
	_(assume exists int v. p |-> v)
	*p = 0;
	_(assert exists int v. p |-> v && v :: low())
	_(assert v == 0) // note: v is now bound to the value of *p
	return *p;
}

void baz(int *p)
	_(maintains exists int v. p |-> v)
{
	_(assert exists int v. v == 0)
	_(assert v == 0)
} 

/* globals are unsupported for the moment,
   and will eventually be moved to the heap,
   so that access can be shared
int _global;

int global()
	_(ensures result == _global)
{
	return _global;
} */

struct list {
	int value;
	struct list *next;
};

int head(struct list *l)
	_(maintains exists int v. &l->value |-> v)
	_(ensures   result == v)
{
	return l->value;
}

struct list requests;

/* int first_request()
	_(requires exists int v. &requests.value |-> v)
	_(ensures  &requests.value |-> v)
	_(ensures  result == v)
{
	return head(&requests);
} */

int add(int x, int y)
	_(requires y >= 0)
	_(requires y :: low())
	// _(ensures  result == x + y)
{
	int x0 = x;
	int y0 = y;
	while(y > 0)
		_(invariant y >= 0)
		_(invariant y :: low())
		_(invariant x0 + y0 == x + y)
	{
		x++; y--;
	}

	return x;
}

void loop1(int *p)
	_(requires exists int v. p |-> 0)
	_(ensures  exists int w. p |-> w && v <= w)
{
	while(1)
		_(invariant exists int w. p |-> w)
	{
	}
}

int loop2(int y)
	// _(fails incorrect)
	_(requires y :: low() && y >= 0)
	_(requires exists int y0. y0 == y)
	_(ensures  result == y0)
{
	int x = 0;

	while(y > 0)
		_(invariant x + y == y0)
		_(invariant y :: low() && y >= 0)
	{
	  x = x + 1;
	  y = y - 1;
	}
	
	return x;
	// _(assert x == 0)
}

void loop3(int n)
	_(requires n :: low)
{
	int i = 0;
	int *p = alloc();
	int *q = alloc();
	*q = 2;
	while(i < n)
		_(invariant i :: low)
		_(invariant q |-> 2)
		_(invariant exists int v. p |-> v)
	{ *p = *q; i++; }
	*p = 1;
	_(assert q |-> 2)
	free(p);
	free(q);
}

void loop3_incorrect(int n)
	_(requires n :: low)
	_(fails incorrect)
{
	int i = 0;
	int *p = alloc();
	int *q = alloc();
	*q = 2;
	while(i < n)
		_(invariant i :: low)
		_(invariant exists int v. p |-> v)
		_(invariant exists int v. q |-> v)
	{ *p = 0; *q = 3; i++; }
	*p = 1;
	_(assert q |-> 2)
	free(p);
	free(q);
}

void loop4()
	_(fails memory)
{
	int *p; // not allocated
	while(1) *p = 0;
}

int *alloc();
	_(ensures exists int v. result |-> v)

void free(int *x);
	_(requires exists int v. x |-> v)

void secure1(int *p)
	_(requires exists int v.
		p |-> v)
	_(ensures exists int v.
		p |-> v && v :: low())
{
	*p = 0;
}

void secure2(int *out, int in)
	_(requires in :: low())
	_(requires exists int v.
		out |->[low()] v)
	_(ensures
		out |->[low()] in)  
{
	*out = in;
}

int secure3()
	_(ensures result :: low())
{
	int *x = alloc();
	secure1(x);
	int y = *x;
	free(x);
	return y;
}

void secure4(int y)
	_(requires y :: low())
{
	if(y) {}
}

void insecure1(int *p)
	_(fails insecure)
	_(requires exists int v.
		p |-> v)
	_(ensures
		p |-> v && v :: low())
{
	*p = *p;
}

void insecure2(int *out, int in)
	_(fails insecure)
	_(requires in :: high())
	_(requires exists int v.
		out |->[low()] v)
	_(ensures
		out |->[low()] in)  
{
	*out = in;
}


int insecure3(int z)
	_(fails insecure)
	_(ensures result :: low())
{
	int *x = alloc();
	secure1(x);
	int y = *x + z;
	return y;
}

void insecure4(int y)
	_(fails insecure)
{
	if(y) {}
}

struct lock;

int *REG;
	// _(resource exists int v. REG |->[low()] v)

struct lock *l;

void lock(struct lock *l);
	_(ensures exists int v. REG |->[low()] v)

void unlock(struct lock *l);
	_(requires exists int v. REG |->[low()] v)

void thread() {
	lock(l);
	*REG = 0;
	unlock(l);
}

/* Doesn't work in Prabawa et al, VMCAI 2018
   Can be proved easily by SecC */
int fig4(int p)
{
  int b = (p == p);
  if(b) return 10; else return 9;
}

void conditional(int b, int *p)
	_(requires b :: low())
	_(requires b != 0 ==> exists int v. p |-> v)
	_(ensures  b != 0 ==> p |-> 0) 
{
	if(b) *p = 0;
}

void assign(int *p, int x)
	_(requires exists sec s, int v. p |->[s] v)
	_(requires x :: s)
	_(ensures p |->[s] x)
{
	*p = x;
}

void assign_insecure(int *p, int x)
	_(fails insecure)
	_(requires exists sec s, int v. p |->[s] v)
	_(requires x :: high())
	_(ensures p |->[s] x)
{
	*p = x;
}

void false_positive(int *p)
  _(fails incorrect)
  _(requires exists int v. p |-> v && p :: low() && v :: low())
  _(ensures p |->[low()] v)
{
  *p = 0;
}

void calls_fail()
  _(fails memory)
{
  return aux();
}


void test_bind1_checked(int * x, int n);
  _(requires n == 0 ==> x |-> n)


/* this should fail because, at present, proving the precondition
   requires splitting on (n == 0) which is not known to be low */
void test_bind1_checked_insecure(int *x, int n)
  _(requires x |-> n)
  _(fails insecure)
{
  test_bind1_checked(x,n);
}

/* note that the x |-> n gets consumed only when n == 0. Hence,
   we still need to mention it in the postcondition otherwise. */
void test_bind1_checked_secure(int *x, int n)
  _(requires x |-> n)
  _(requires (n == 0) :: low)
  _(ensures n != 0 ==> x |-> n)  
{
  test_bind1_checked(x,n);
}

/* this program is designed to illustrate the unsoundness that can
 * arise if we do not carefully check that assertions that will be
 * case split upon are low. In particular, if we make a case
 * distinction on x > 0, then in either case (x > 0)::low is
 * provable. Once we have (unsoundly) established that fact, we can
 * then of course falsely claim to return low data */
int check_split_insecure_consume(int x)
  _(ensures result :: low)
  _(fails insecure) // XXX: for technical reasons, this is not flagged as insecure but incorrect
{
  _(assert(x > 0 ? (x > 0)::low : (x > 0)::low))
  return (x > 0);
}


/* this might seem a bit harsh to disallow. Unlike the example
 * above we are now using an assume statement. In some sense, if
 * you use an assume statement you are being dangerous. However,
 * we would like to limit the degree of danger. Specifically,
 * we want to avoid the case where we assume something and
 * then, subsequently, do unsound reasoning (regardless of whether
 * that assumption was valid or not). Reasoning from conditional
 * assertions is by case analysis and, as we saw above, that is
 * sound only when the condition is low. Hence, why this is
 * rightly judged insecure. */ 
int check_split_insecure_produce(int x)
  _(ensures result :: low)
  _(fails insecure) // XXX: the above currently applies only for spatial assertions, pure and relational ones are not split explicitly
{
  _(assume(x > 0 ? (x > 0)::low : (x > 0)::low))
  return (x > 0);
}

/* lemma is illegal since it has side effects, which can lead to unsoundness */
void illegal_lemma(int *a, int i)
_(lemma)
_(fails effects)
_(requires exists int v. a |-> v)
_(ensures a |-> i)
{
  if ((*a = i) == i){
  }
}

/* demonstrates unsoundness of lemmas with side effects. Specifically,
 * if the call to illegal_lemm() is omitted, the deduction is clearly
 * unsound. But this is precisely the purpose of _(apply ...): to allow
 * such calls to be omitted by the compiler. */
void use_illegal_lemma(int *a, int i)
  _(requires exists int v. a |-> v)
  _(ensures a |-> i)
  _(fails effects)
{
  _(apply illegal_lemma(a,i);)
}

/* this lemma is OK and indeed we will probably want lemmas to be
 * able to inspect the heap, so demanding absence of all effects
 * is too stringent. Instead, absence of state modification would
 * appear to be the correct restriction.*/
void legal_lemma(int *a, int i)
_(lemma)
     _(requires exists int v. a |-> v && v :: low)
_(ensures a |-> v)
{
  int x = *a;
  if (x){
  }
}


/* Previously, SecC was conservative and considered the write below
 * to deem the lemma illegal, even though it does not introduce
 * unsoundness.
 * This restriction is now relaxed, and x is (temporarily) ghost state.
 */
void illegal_lemma2()
  _(lemma)
  // _(fails effects)
{
  int x = 0;
  x++;
}

/* test for side-effects during lemma application */
void do_nothing(int i)
  _(lemma)
{
}

int broken_apply()
  _(ensures result == 1)
  _(fails effects)
{
  int i = 0;
  _(apply do_nothing(i++);)
    return i;
}

/* tests for predicate consumption */
_(predicate inv(int *p) (exists int v. p |-> v))

_(predicate blah(int *l, int *p) (exists int _l. l |-> _l && (_l == 1 ==> inv(p))))


/* this should successfully pass. In particular, since l |-> 0, folding
   blah should not consume inv(p) */
void func(int *p, int * l)
_(requires inv(p))
_(requires exists int _lv. l |-> _lv && _lv :: low && _lv == 0)
_(ensures inv(p))
_(ensures blah(l,p))
{
  _(fold blah(l,p))
}

/* this should fail. In particular, since l |-> 0, folding
   blah should yield at least one state that is satisfiable in which
   inv(p) does not hold (because it never held in the precondition) */
void func3(int *p, int * l)
  _(requires exists int _lv. l |-> _lv && _lv :: low && _lv == 0)
  _(ensures inv(p))
  _(ensures blah(l,p))
  _(fails memory)
{
  _(fold blah(l,p))
}

/* this should fail. Because l |-> 1, folding blah should consume inv,
   meaning the postcondition cannot be satisfied */
void func2(int *p, int * l)
  _(requires inv(p))
  _(requires exists int _lv. l |-> _lv && _lv :: low && _lv == 1)
  _(ensures inv(p))
  _(ensures blah(l,p))
  _(fails memory)
{
  _(fold blah(l,p))
 }

void disjoint(int * x)
  _(requires exists int v. x |-> v)
  _(requires exists int v2. x |-> v2)
  _(ensures (1 == 0))
  _(lemma)
{
}


void disjoint2()
  _(requires exists int *p, int _p. p |-> _p)
  _(requires exists int *q, int _q. q |-> _q)
  _(requires p == q)
  _(ensures (1 == 0))
  _(lemma)
{
}

void logical_var_lemma(int x)
  _(lemma)
  _(requires x > 0)
{
}

/* allowed to pass logical variables to _apply */
void logical_var_good(){
  _(assert exists int x. x > 0)
  _(apply logical_var_lemma(x);)
}


void logical_var_func(int x)
  _(requires x > 0)
{
}

/* not allowed to use logical variables in code */
void logical_var_bad()
  _(fails effects)
{
  _(assert exists int x. x > 0)
  logical_var_lemma(x);
}


 void logical_var_branch(){
  _(assert exists int x. x > 0)
    _(apply if (x <= 0) { _(assert (1 == 0)) })
}


int inc(int i){ return i++; }

/* this should fail since we are calling a non-lemma function in the apply */
void apply_inc(int i)
  _(fails effects)
{  
  _(apply if (i == i) inc(i);)
}

/* test case for conditional proof strategies */
_(predicate ptr(int *p, sec l, int v)
  p |-> v && v :: l)

_(predicate ptr_sec(int *p, int lev, int v)
  lev :: low && (lev == 0 ==> ptr(p,low, v)))

void test_branching(int *p)
_(requires exists int lev, int v. ptr_sec(p, lev, v))
_(ensures ptr_sec(p, lev, v))
{
  _(unfold ptr_sec(p, lev, v))
  _(apply
    if(lev == 0) {
      _(unfold ptr(p,low, v))
      if (v > 0) {
        _(fold ptr(p,low, v))
      } else {
        _(fold ptr(p,low, v))
      }
    }
   )
  _(fold ptr_sec(p, lev, v))
}

/* Issue 15: bug in the parser */
typedef struct {
  int *data;        /* the backing memory */
} buf_t;

void get(buf_t *b, int i, int *out)
  _(requires exists int * _data. &b->data |-> _data)
  _(requires exists int x. _data + i |-> x)
  _(requires exists int _old. out |-> _old)
  _(ensures out |-> x)
  _(ensures &b->data |-> _data)
  _(ensures _data + i |-> x)
{
  *out = b->data[i];
}

_(constant
	int zero = 0)

_(constant
	int nonzero)

_(axioms
	zero != nonzero)

_(function
	int max(int n, int m))

_(function
	int inc(int i) = i + 1)

_(rewrites
	forall int n, int m. n < m ==> max(n, m) == m)

int fundef(int n)
	_(ensures result == inc(n))
{
	return n + 1;
}

int rw(int n, int m)
	_(requires n < m)
	_(ensures result == max(n, m))
{
	return m;
}

void ax_rw()
	_(ensures nonzero != zero)
{
}

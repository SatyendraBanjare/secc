#include "secc.h"

_(predicate str(int *a; int n)
	n == 0 ? a |-> 0 : n > 0 && exists int x. a |-> x && x != 0 && str(a + 1; n - 1))

_(predicate ar(int *a, int n)
	n > 0 ==> exists int x. a |-> x && ar(a + 1, n - 1))

void test_eq(int *x, int *y)
	_(requires x == y)
	_(requires exists int v. x |-> v)
	_(ensures  y |-> v)
{
}

void split(int *a, int i, int n)
	_(lemma)
	_(requires i :: low)
    _(requires 0 <= i && i <= n)
	_(requires ar(a, n))
	_(ensures  ar(a, i) && ar(a + i, n - i))
{
	if(i == 0) {
		_(fold ar(a, 0))
	} else {
		_(unfold ar(a, n))
        _(apply split(a+1, i-1, n-1);)
		_(fold ar(a, i))
	}
}

int strlen(int *a)
	_(requires exists int n. str(a; n) && n :: low())
	_(ensures                str(a; n))
	_(ensures  result == n)
{
	_(unfold str(a; n))
	int res;

	if(*a != 0) {
		res = strlen(a + 1) + 1;
	} else {
		res = 0;
	}

	_(fold str(a; n))
	return res;
}

int strlen_insecure(int *a)
	_(requires exists int n. str(a; n))
	_(ensures                str(a; n))
	_(ensures  result == n)
	
	/* The reason why this should fail is that n is not public,
	 * which leaks into the branching condition of str.
	 */
	_(fails    insecure)
{
	_(unfold str(a; n))
	int res;

	if(*a != 0) {
		res = strlen_insecure(a + 1) + 1;
	} else {
		res = 0;
	}

	_(fold str(a; n))
	return res;
}

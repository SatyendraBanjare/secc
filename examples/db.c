#include "secc.h"

/* here we use SecC to specify some confidentiality properties of some
   query-style functions. For simplicity, we consider associative arrays
   that store (key,value) pairs. Each array may contain multiple elements
   that share a common key. We use SecC's value-dependent classification
   assertions to specify that query operations should return only derived
   only from those values associated with the given key. 
   
   To do that we write a precondition that says that the only low values
   in the array are those associated with the given key, and we require
   the results returned to be low */
typedef struct {
  int key;
  int value;
} elem_t;

/* An array of elems of length n storing  arbitrary values.
 * Describes the empty heap region if n <= 0 */
_(predicate ar(elem_t *a, int n)
  n > 0 ==> exists int k, int v. &a->key |-> k && &a->value |-> v && ar(a + 1, n - 1))

/* this version of the ar predicate says that all values are high except
   that one associated with the given key. We use it to specify that
   the lookup function cannot return any other information except that
   associated with the given key. */
_(predicate ar_lookup(elem_t *a, int n; int key)
  n > 0 ==> exists int k, int v. &a->key |-> k && &a->value |-> v && k :: low && (k == key ==> v :: low) && ar_lookup(a + 1, n - 1; key))


/* this version of the ar predicate says that all values are low except
   the ones associated with the given key. It is used to specify that
   entries are correctly removed from the array. */
_(predicate ar_delete(elem_t *a, int n; int key)
  n > 0 ==> exists int k, int v. &a->key |-> k && &a->value |-> v && k :: low && (k != key ==> v :: low) && ar_delete(a + 1, n - 1; key))

/* all entries are low -- is what should hold after deleting */
_(predicate ar_low(elem_t *a, int n)
  n > 0 ==> exists int k, int v. &a->key |-> k && &a->value |-> v && k :: low && v :: low && ar_low(a + 1, n - 1))

int SUCCESS = 0;
int FAILURE = 1;

void ar_lookup_snoc(elem_t *a, int n, int key)
_(requires n >= 0)
_(requires n :: low)
_(requires key :: low) // Note: implied by ar_lookup, but not visible for the implication in the next line
_(requires ar_lookup(a,n;key))
_(requires exists int k, int v. &(a+n)->key |-> k && &(a+n)->value |-> v && k :: low && (k == key ==> v :: low))
_(ensures ar_lookup(a,n+1;key))
_(lemma)
{
  if (n == 0){
    _(unfold ar_lookup(a,n;key))
    _(fold ar_lookup(a+1,0;key))
    _(fold ar_lookup(a,1;key))
  }else{
    _(unfold ar_lookup(a,n;key))
    _(apply ar_lookup_snoc(a+1,n-1,key);)
    _(fold ar_lookup(a,n+1;key))
  }
}

void ar_lookup_join(elem_t *a, int n, int key, int m)
_(requires n >= 0 && m >= 0)
_(requires n :: low && m :: low)
_(requires key :: low)
_(requires ar_lookup(a,n;key) && ar_lookup(a+n,m;key))
_(ensures ar_lookup(a,n+m;key))
_(lemma)
{
  if (n == 0){
    _(unfold ar_lookup(a,n;key))
  }else{
    _(unfold ar_lookup(a,n;key))
    _(apply ar_lookup_join(a+1,n-1,key,m);)
    _(fold ar_lookup(a,n+m;key))
  }
}

void ar_low_snoc(elem_t *a, int n)
_(requires n >= 0)
_(requires n :: low)
_(requires ar_low(a,n))
_(requires exists int k, int v. &(a+n)->key |-> k && &(a+n)->value |-> v && k :: low && v :: low)
_(ensures ar_low(a,n+1))
_(lemma)
{
  if (n == 0){
    _(unfold ar_low(a,n))
    _(fold ar_low(a+1,0))
    _(fold ar_low(a,1))
  }else{
    _(unfold ar_low(a,n))
    _(apply ar_low_snoc(a+1,n-1);)
    _(fold ar_low(a,n+1))
  }
}


/* returns SUCCESS/FAILURE. On SUCCESS *valueOut holds the value corresponding
 * to the given key. The code is a little awkward becuase we don't yet
 * support return statements in loop bodies */
int lookup(elem_t *elems, int len, int key, int *valueOut)
  _(requires ar_lookup(elems,len;key))
  _(requires len :: low)
  _(requires key :: low)
  _(requires len >= 0)
  _(requires exists int oldOut. valueOut |-> oldOut)
  _(requires exists sec lev. oldOut :: lev)
  _(ensures exists int out. valueOut |-> out)
  _(ensures result == SUCCESS ==> out :: low)
  _(ensures result == FAILURE ==> out :: lev)
  _(ensures ar_lookup(elems,len;key))
{
  int i = 0;
  elem_t *p = elems;
  int ret = FAILURE;
  _(fold ar_lookup(elems,0;key))
  while (i < len && ret == FAILURE)
    _(invariant i >= 0 && i <= len)
    _(invariant ret :: low && i :: low)
    _(invariant ret == SUCCESS ==> exists int v. valueOut |-> v && v :: low) 
    _(invariant ret == FAILURE ==> valueOut |-> oldOut)
    _(invariant ret >= SUCCESS && ret <= FAILURE)
    _(invariant ar_lookup(p,len-i;key))
    _(invariant ar_lookup(elems,i;key))
    _(invariant p == elems + i)
    {
    _(unfold ar_lookup(p,len - i;key))
    if (p->key == key){
      *valueOut = p->value;
      ret = SUCCESS;
    }
    p++;
    _(apply ar_lookup_snoc(elems,i,key);)    
    i++;
  }
  _(apply ar_lookup_join(elems,i,key,len-i);)
  return ret;
}

void split(elem_t *a, int i, int n)
  _(lemma)
	_(requires i :: low)
    _(requires 0 <= i && i <= n)
    _(requires exists int key. ar_lookup(a, n; key) && key :: low)
    _(ensures  ar_lookup(a, i; key) && ar_lookup(a + i, n - i; key))
{
	if(i == 0) {
          _(fold ar_lookup(a, 0; key))
	} else {
          _(unfold ar_lookup(a, n; key))
          _(apply split(a+1, i-1, n-1);)
          _(fold ar_lookup(a, i; key))
	}
}

void expose(elem_t *a, int i, int n)
    _(lemma)
	_(requires i :: low)
    _(requires 0 <= i && i < n)
     _(requires exists int key. ar_lookup(a, n; key) && key :: low)
     _(ensures  exists int k, int v. &(a + i)->key |-> k && &(a + i)->value |-> v && k :: low && (k == key ==> v :: low))
     _(ensures  ar_lookup(a, i; key) && ar_lookup(a + i + 1, n - i - 1; key))
{
	if(i == 0) {
          _(unfold ar_lookup(a, n; key))
          _(fold ar_lookup(a, 0; key))
	} else {
	  _(unfold ar_lookup(a, n; key))
          _(apply expose(a+1, i-1, n-1);)
          _(fold ar_lookup(a, i; key))
	}
}

void cover(elem_t *a, int i, int n)
    _(lemma)  
	_(requires i :: low)
    _(requires 0 <= i && i < n)
     _(requires exists int key. ar_lookup(a, i; key) && ar_lookup(a + i + 1, n - i - 1;key) && key :: low)
     _(requires exists int k, int v. &(a + i)->key |-> k && &(a + i)->value |-> v && k :: low && (k == key ==> v :: low))
     _(ensures  ar_lookup(a, n; key))
{
	if(i == 0) {
          _(unfold ar_lookup(a, 0; key))
          _(fold   ar_lookup(a, n; key))
	} else {
	  _(unfold ar_lookup(a, i; key))
          _(apply cover(a+1, i-1, n-1);)
          _(fold ar_lookup(a, n; key))
	}
}

/* the same spec holds for binary search */
int binsearch(elem_t *elems, int len, int key, int *valueOut)
  _(requires ar_lookup(elems,len;key))
  _(requires len :: low)
  _(requires key :: low)
  _(requires exists int oldOut. valueOut |-> oldOut)
  _(requires exists sec lev. oldOut :: lev)
  _(ensures exists int out. valueOut |-> out)
  _(ensures result == SUCCESS ==> out :: low)
  _(ensures result == FAILURE ==> out :: lev)
  _(ensures ar_lookup(elems,len;key))
{
  if (len <= 0){
    return FAILURE;
  }
  int mid = len/2;
  _(apply expose(elems,mid,len);)
  /* XXX: note calling this k causes an assertion failure in SecC! */
  int thek = (elems + mid)->key;
  if (thek == key){
    *valueOut = (elems + mid)->value;
    _(apply cover(elems,mid,len);)
    return SUCCESS;
  }else{
    if (len == 1){
      _(apply cover(elems,mid,len);)      
      return FAILURE;
    }
    if (thek > key){
      _(apply cover(elems,mid,len);)
      _(apply split(elems,mid-1,len);)
      int ret = binsearch(elems,mid-1,key,valueOut);
      _(apply ar_lookup_join(elems,mid-1,key,len-(mid-1));)
      return ret;
    }else{
      _(apply cover(elems,mid,len);)
      _(apply split(elems,mid+1,len);)
      int ret = binsearch(elems+mid+1,len - mid - 1,key,valueOut);
      _(apply ar_lookup_join(elems,mid+1,key,len-(mid+1));)
      return ret;
    }
  }
}

int sum_all(elem_t *elems, int len, int key)
  _(requires ar_lookup(elems,len;key))
  _(requires len :: low)
  _(requires key :: low)
  _(requires len >= 0)
  _(ensures result :: low)
  _(ensures ar_lookup(elems,len;key))
{
  int sum = 0;
  elem_t *p = elems;
  int i = 0;
  _(fold ar_lookup(elems,0;key))
  while (i < len)
    _(invariant i >= 0 && i <= len)
    _(invariant sum :: low && i :: low)
    _(invariant ar_lookup(p,len-i;key))
    _(invariant ar_lookup(elems,i;key))
    _(invariant p == elems + i)
    {
    _(unfold ar_lookup(p,len - i;key))
    if (p->key == key){
      sum += p->value;
    }
    p++;
    _(apply ar_lookup_snoc(elems,i,key);)    
    i++;
  }
  _(apply ar_lookup_join(elems,i,key,len-i);)
  return sum;
}


/* returns the sum of all values associated to the given key. 0 otherwise
   this shows in some sense the ease of the recursive reasoning. */
int sum_all_rec(elem_t *p, int len, int key, int init)
  _(requires ar_lookup(p,len;key))
  _(requires init :: low)
  _(requires len :: low)
  _(requires len >= 0)
  _(requires key :: low)
  _(ensures ar_lookup(p,len;key))
  _(ensures result :: low)
{
  if (len > 0)
  {
    _(unfold ar_lookup(p,len;key))
    if (p->key == key){
      int s = sum_all_rec(p+1,len-1,key,init + p->value);
      _(fold ar_lookup(p,len;key))
      return s;
    }else{
      int s = sum_all_rec(p+1,len-1,key,init);
      _(fold ar_lookup(p,len;key))
      return s;
    }
  }else{
    return init;
  }
}



void remove_all(elem_t *elems, int len, int key)
  _(requires ar_delete(elems,len;key))
  _(requires len :: low && key :: low && len >= 0)
  _(ensures ar_low(elems,len))
{
  int i = 0;
  elem_t *p = elems;
  _(fold ar_low(elems,0))
  while (i < len)
    _(invariant i :: low && i >= 0 && i <= len)
    _(invariant ar_low(elems,i))
    _(invariant ar_delete(p,len-i;key))
    _(invariant p == elems + i)
  {
    _(unfold ar_delete(p,len-i;key))
    if (p->key == key){
      p->value = 0;
    }
    _(apply ar_low_snoc(elems,i);)
    p++;
    i++;
  }
  _(unfold ar_delete(p,len-i;key))
}


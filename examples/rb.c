#include "secc.h"

/* An array of integers of length n storing  arbitrary values of a 
 * given security level.
 * Describes the empty heap region if n <= 0 */
_(predicate ar_sec(int *a, int n, sec l)
  n > 0 ==> exists int x. a |-> x && x :: l && ar_sec(a + 1, n - 1, l))

/* An array of integers of length n storing  arbitrary values 
 * Describes the empty heap region if n <= 0 */
_(predicate ar(int *a, int n)
  n > 0 ==> exists int x. a |-> x && ar(a + 1, n - 1))

/* Helper lemma function that splits an array at a given index i
 * such that a[i] = ... is proved memory safe. */
void expose(int *a, int i, int n)
    _(lemma)
    _(requires i :: low)
    _(requires 0 <= i && i < n)
    _(requires ar(a, n))
    _(ensures  exists int v. (a + i) |-> v)
    _(ensures  ar(a, i) && ar(a + i + 1, n - i - 1))
{
    if(i == 0) {
        _(unfold ar(a, n))
        _(fold ar(a, 0))
    } else {
        _(unfold ar(a, n))
        _(apply expose(a+1, i-1, n-1);)
        _(fold ar(a, i))
    }
}

/* Helper lemma function that unsplits an array at a given index i,
 * useful after a call to expose (see above). */
void cover(int *a, int i, int n)
    _(lemma)  
       _(requires i :: low)
    _(requires 0 <= i && i < n)
       _(requires exists int v. (a + i) |-> v)
       _(requires ar(a, i) && ar(a + i + 1, n - i - 1))
       _(ensures  ar(a, n))
{
       if(i == 0) {
               _(unfold ar(a, 0)) // show that the first segment is empty
               _(fold   ar(a, n))
       } else {
               _(unfold ar(a, i))
                _(apply cover(a+1, i-1, n-1);)
               _(fold ar(a, n))
       }
}

/* a ringbuffer in which the read index is kept separate.
 * that is done so it can be used as a data diode, if needed, whereby
 * reading from the buffer doesn't reveal anything to the sender.
 * Here we have an array of ints but one could use a larger element type
 * in practice */

typedef struct {
  int *data;        /* the backing memory */
  int capacity;     /* Previously: Must be a power of two, now scrapped, using modulo */
  int write_index;  /* intentionally not wrapped to capacity */
} rb_t;

/* The corresponding heap abstraction where the values of the fields are given
 * by the predicate's parameters.
 *
 * Additional invariants imposed:
 * - _data is an array with _capacity
 * - _write_index is not negative
 * - _capacity is strictly positive (not yet: power of two)
 */
_(predicate rb(rb_t *buf; int *_data, int _capacity, int _write_index, int _read_index, sec l)
    &buf->data        |-> _data &&
    &buf->capacity    |-> _capacity &&
    &buf->write_index |-> _write_index &&
   _capacity :: low && _write_index :: low && _read_index :: low &&
    0 <= _write_index && 0 < _capacity && 0 <= _read_index &&
    _read_index <= _write_index &&
  (
    _read_index % _capacity <= _write_index % _capacity ==>
        ar(_data,_read_index % _capacity) &&
        ar_sec(_data + _read_index % _capacity, _write_index % _capacity - _read_index % _capacity,l) &&
        ar(_data + _write_index % _capacity, _capacity - _write_index % _capacity)
  )
  &&
  (
    _write_index % _capacity < _read_index % _capacity ==> 
        ar_sec(_data, _write_index % _capacity, l) &&
        ar(_data + _write_index % _capacity, _read_index % _capacity - _write_index % _capacity) &&
        ar_sec(_data + _read_index % _capacity, _capacity - _read_index % _capacity, l)
  ) 

 )

/* returns 0 on success. Note capacity specifies the capacity as a power
   of two. It must be < 32 */
int rb_init(rb_t * buf, int capacity, int * backing)
  _(requires exists int * _data, int _capacity, int _write_index.
        &buf->data        |-> _data &&
        &buf->capacity    |-> _capacity &&
        &buf->write_index |-> _write_index &&
        capacity :: low)
  _(requires 0 < capacity)
  _(requires ar(backing, capacity))
  _(ensures
    rb(buf; backing, capacity, 0, 0, low))
{
  buf->data = backing;
  buf->capacity = capacity;
  buf->write_index = 0;
  _(fold ar(backing,0))
  _(fold ar_sec(backing,0,low))
  _(fold rb(buf; backing, capacity, 0, 0, low))
  return 0;
}

void concat_ar(int *a, int n1, int n2)
  _(lemma)
  _(requires ar(a, n1) && n1 :: low && n1 >= 0)
  _(requires ar(a+n1, n2) && n2 :: low && n2 >= 0)
  _(ensures ar(a, n1+n2))
{
  if (n1 <= 0){
    _(unfold ar(a, n1))
    _(unfold ar(a+n1, n2))
      _(fold ar(a, n1+n2))
  } else {
    _(unfold ar(a, n1))
    concat_ar(a+1,n1-1,n2);
    _(fold ar(a, n1+n2))
  }
}

void ar_read(int *a, int n)
  _(lemma)
  _(requires ar(a,n) && n :: low && n >= 0)
  _(requires exists int v. (a + n) |-> v)
  _(ensures ar(a,n+1))
{
  if (n == 0){
    _(unfold ar(a,n))
    _(fold ar(a+1,0))
    _(fold ar(a,n+1))
  } else {
  _(unfold ar(a,n))
  ar_read(a+1,n-1);
  _(fold ar(a,n+1))
  }
}

void ar_sec_put(int *a, int n, sec l)
  _(lemma)
  _(requires ar_sec(a,n,l) && n :: low && n >= 0)
  _(requires exists int v. (a + n) |-> v && v :: l)
  _(ensures ar_sec(a,n+1,l))
{
  if (n == 0){
    _(unfold ar_sec(a,n,l))
    _(fold ar_sec(a+1,0,l))
    _(fold ar_sec(a,n+1,l))
  } else {
  _(unfold ar_sec(a,n,l))
  ar_sec_put(a+1,n-1,l);
  _(fold ar_sec(a,n+1,l))
  }
}

void arsec2ar(int *a, int n, sec l)
    _(lemma)
    _(requires ar_sec(a,n,l) && n :: low)
    _(ensures ar(a,n))
{
  _(unfold ar_sec(a,n,l))
  if (n <= 0){
    _(fold ar(a,n))
  }else{
    _(apply arsec2ar(a+1,n-1,l);)
    _(fold ar(a,n))
  }
}


void mod_def(int x, int c, int k)
  _(lemma)
  _(requires x%c==k && x >= 0 && c > 0)
  _(ensures exists int _n. _n >= 0 && x == _n*c + k)
{
}

void blah(int c, int n, int _n, int k, int _k)
  _(lemma)
  _(requires (n*c)+k == (_n*c)+_k)
  _(requires k >= 0 && _k >= 0 && k < c && _k < c)
  _(requires n >= 0 && _n >= 0 && c > 0)
  _(requires n :: low)
  _(ensures n == _n && k == _k)
{
  if (n == 0) {
  } else {
    blah(c,n-1,_n-1,k,_k);
  }
}


void mod_def_rev(int x, int n, int c, int k)
  _(lemma)
  _(requires x >= 0 && n >= 0 && c > 0 && k >= 0 && k < c)
  _(requires n :: low)
  _(requires x == n*c + k)
  _(ensures x%c == k)
{
  _(apply mod_def(x,c,x%c);)
  _(assert exists int _n. x == (_n * c) + (x%c) && _n >= 0)
  _(apply blah(c,n,_n,k,x%c);)
}

void mod_plusone(int x, int c)
  _(lemma)
  _(requires x :: low && c :: low)
  _(requires x >= 0 && c > 0 && (x % c) < (c-1))
  _(ensures (x % c) + 1 == (x + 1) % c)
{
  mod_def(x,c,x%c);
  _(assert exists int k. x == k*c + (x%c) && k >= 0)
  _(assert k :: low)
  _(assert (x+1) == k*c + ((x%c) + 1))
  mod_def_rev(x+1,k,c,(x%c)+1);
}

void mod0(int x, int c, int n)
  _(lemma)
  _(requires x >= 0 && c > 0)
  _(requires n >= 0 && (x == n*c))
  _(requires n :: low)
  _(ensures x%c == 0)
{
  _(apply mod_def_rev(x,n,c,0);)
}

void mod_backto0(int x, int c)
  _(lemma)
  _(requires x :: low && c :: low)
  _(requires x >= 0 && c > 0 && x%c == c-1)
  _(ensures (x+1)%c==0 && x >= 0 && c > 0 && x%c == c-1)
{
  _(assert exists int n. n >= 0 && x == n*c + (c-1))
  _(apply mod0(x+1,c,n+1);)
}

/* returns 0 on success. Note capacityPow specifies the capacity as a power
   of two. It must be < 32 */
void rb_put(rb_t * buf, int byte)
  _(requires exists int * _data, int _c, int _w, int _r, sec _l.
    rb(buf; _data, _c, _w, _r, _l))
  _(requires byte :: _l)
  _(ensures exists int * _data.
    rb(buf; _data, _c, _w + 1, _r, _l))
{
  /* Unfold predicate rb to look at the invariant */ 
  _(unfold rb(buf; _data, _c, _w, _r, _l))
  
  /* we don't bother to check how the reader is going, so this could
     overflow if they are not keeping up */
  int index = buf->write_index % buf->capacity;
  
  /* Make sure the array access is proved memory safe */
  //_(assert exists int _n. ar(_data + index,_n) && _n > 0)

  _(apply 
     if (_r % _c <= _w % _c) {          
     _(unfold ar(_data + _w % _c, _c - _w % _c))
     } else {
         _(unfold ar(_data + _w % _c, _r % _c - _w % _c))        
     }
  )      

  buf->data[index] = byte;
  buf->write_index++;

  _(apply 
    if (_r % _c <= _w % _c) {

        if (_w%_c==_c-1) {
            _(apply mod_backto0(_w, _c);)
            if (_r%_c==0) {
              
                // already have [ar(data, r%c)]  [1]
                _(fold ar_sec(_data+_r%_c, (_w+1)%_c-_r%_c, _l)) // [2] since len==0               
                // from ar_sec(data+r%c, w%c-r%c, l), after writing
                _(apply ar_sec_put(_data+_r%_c, _w%_c-_r%_c, _l, _c);)
                // have ar_sec(data+r%c, w%c-r%c+1, l)
                _(apply arsec2ar(_data+_r%_c, _w%_c-_r%_c+1, _l);)
                // have [ar(_data+_r%_c, _w%_c-_r%_c+1)] = [ar(data+r%c, c)]
                _(unfold ar(_data+_r%_c, _w%_c-_r%_c+1))
                _(fold ar(_data+(_w+1)%_c, _c-(_w+1)%_c))  //[3]
                //_(dumpstate blah111)
            } else {
                //_r%c > 0 = (_w+1)%c
                _(fold ar_sec(_data, (_w+1)%_c,_l))   // since len=0
                _(unfold ar(_data, _r%_c))
                _(fold ar(_data + (_w+1)%_c, _r%_c-(_w+1)%_c))  // since ar(data, r%c) and (w+1)%c==0
                _(apply ar_sec_put(_data+_r%_c, _w%_c-_r%_c, _l, _c);)
                // have ar_sec(_data+_r%_c, _w%_c-_r%_c+1, _l)
                _(unfold ar_sec(_data+_r%_c, _w%_c-_r%_c+1, _l))
                _(fold ar_sec(_data+_r%_c, _c-_r%_c, _l))   // since w%c=c-1
                //_(dumpstate blah222) 
            }
        } else {
            // 0 <= w%c < c-1
            // already have [ar(data, r%c)] [1]
            _(apply ar_sec_put(_data+_r%_c, _w%_c-_r%_c, _l, _c);)
            // have ar_sec(_data+_r%_c, _w%_c-_r%_c+1, _l)
            _(apply mod_plusone(_w,_c);)
            // have (w+1)%c == w%c+1
            _(unfold ar_sec(_data+_r%_c, _w%_c-_r%_c+1, _l))
            _(fold ar_sec(_data+_r%_c, (_w+1)%_c-_r%_c, _l)) // [2]
            // we have ar(data+w%c+1, c-w%c-1) and (w+1)%c == w%c+1
            _(unfold ar(_data+_w%_c+1, _c-_w%_c-1))
            _(fold ar(_data+(_w+1)%_c, _c-(_w+1)%_c))  //[3]
            //_(dumpstate blah333)
        }

    } else {
        // w%c < r%c <= c-1, therefore (w%c)+1==(w+1)%c
        if (_r%_c - _w%_c == 1) {
            // have ar_sec(data, w%c, l)
            _(apply ar_sec_put(_data, _w%_c, _l, _c);)
            // have ar_sec(data, w%c+1, l)
            _(apply arsec2ar(_data, _w%_c+1, _l);)
            // have ar(_data, _w%_c+1)
            _(unfold ar(_data, _w%_c+1)) 
            _(fold ar(_data, _r%_c))//[1], since w%c+1==r%c
            _(apply mod_plusone(_w,_c);)
            // have (w+1)%c == w%c+1
            _(fold ar_sec(_data+_r%_c, (_w+1)%_c-_r%_c, _l)) // [2], since len=0
            // we have ar_sec(data+r%c, c-r%c, l) and r%c == w%c+1 == (w+1)%c
            _(unfold ar_sec(_data+_r%_c, _c-_r%_c, _l))
            _(fold ar_sec(_data+(_w+1)%_c, _c-(_w+1)%_c, _l))
            _(apply arsec2ar(_data+(_w+1)%_c, _c-(_w+1)%_c, _l);)
            // have ar(_data+(w+1)%c, c-(w+1)%c) [3]
            //_(dumpstate blah444)
        } else {
            // r%c - w%c >1
            _(apply ar_sec_put(_data, _w%_c, _l, _c);)
            // have ar_sec(_data, _w%_c+1, _l)
            _(apply mod_plusone(_w,_c);)
            // have (w+1)%c == w%c+1
            _(unfold ar_sec(_data, _w%_c+1, _l))
            _(fold ar_sec(_data, (_w+1)%_c, _l))  // [1]
            // after unfolding already have ar(data+w%c+1, r%c-w%c-1)
            _(unfold ar(_data+_w%_c+1, _r%_c-_w%_c-1))
            _(fold ar(_data+(_w+1)%_c, _r%_c-(_w+1)%_c))  // [2]
            // already have ar_sec(data+r%c, c-r%c, l) [3]
            //_(dumpstate blah555)
        }     
    }
  )
  /* Fold back */ 
  _(fold rb(buf; _data, _c, _w + 1, _r, _l))
  //_(assert dump)
  _(apply
        if (_c-_w%_c-1==0) {
            _(unfold ar(_data+_w%_c+1, _c-_w%_c-1))
        }       
    )
  _(apply
        if (_r%_c-_w%_c-1==0) {
            _(unfold ar(_data+_w%_c+1, _r%_c-_w%_c-1))   // in case 4
        }
    )

}

/* returns 0 on success, nonzero on failure, e.g. empty buffer */
int rb_get(rb_t * buf, int * read_index, int * out)
  _(requires exists int * _data, int _c, int _w, int _r, sec _l, sec _l2.
    rb(buf; _data, _c, _w, _r, _l))
  _(requires read_index |-> _r) 
  _(requires exists int _old_out. out |-> _old_out && _old_out :: _l2 )
  
  _(ensures exists int * _data, int _new_r.
    rb(buf; _data, _c, _w, _new_r, _l))
  _(ensures read_index |-> _new_r) 
  _(ensures (result == 0) ? (_new_r == _r + 1) : (_new_r==_r) )
  _(ensures exists int _out. out |-> _out && (_out :: (result == 0 ? _l : _l2) )) // need to also describe _out when get fails?
{
  _(unfold rb(buf; _data, _c, _w, _r, _l))
  /* we check only for not-equal here, rather than e.g. <, since we
     cannot assume that e.g. the write_index hasn't wrapped around
     (assuming the writer is keeping up). */
  if ((*read_index) % buf->capacity != buf->write_index % buf->capacity) {
    
    int index = (*read_index) % buf->capacity;
    /* Make sure the array access is proved memory safe */

    // show index is indeed pointing at something so we can load data[index]
    _(apply 
        if (_r%_c < _w%_c) {
            _(unfold ar_sec(_data+_r%_c, _w%_c-_r%_c, _l))
        } else {
            _(unfold ar_sec(_data+_r%_c, _c-_r%_c, _l))
        }
    )

    (*out) = ((buf->data)[index]);
    *read_index = *read_index + 1;

    /* Fold back */
    _(apply 
        if (_r%_c < _w%_c) {
            // already have ar(data,r%c)
            _(apply ar_read(_data, _r%_c);)
            // have ar(data, r%c+1)
            _(apply mod_plusone(_r, _c);)
            // have r%c+1 == (r+1)%c
            _(unfold ar(_data, _r%_c+1))
            _(fold ar(_data, (_r+1)%_c))  // [1]
            // after unfolding already have ar_sec(data+r%c+1, w%c-r%c-1, l)
            _(unfold ar_sec(_data+_r%_c+1, _w%_c-_r%_c-1, _l))
            _(fold ar_sec(_data+(_r+1)%_c, _w%_c-(_r+1)%_c, _l)) // [2]
            // already have ar(data+w%c, c-w%c) after unfolding rb()  [3]
            //_(dumpstate blah111)
        } else {
            // w%c < r%c
            if (_r%_c == _c-1) {
                _(apply mod_backto0(_r, _c);)
                // (r+1)%c == 0
                _(fold ar(_data, (_r+1)%_c)) // since len=0, [1]
                // already have ar_sec(data, w%c, l)
                _(unfold ar_sec(_data, _w%_c, _l))
                _(fold ar_sec(_data+(_r+1)%_c, _w%_c-(_r+1)%_c, _l))  // [2]
                // already have ar(_data+_w%_c, _r%_c-_w%_c)
                _(unfold ar(_data+_w%_c, _r%_c-_w%_c))
                _(fold ar(_data+_w%_c, _c-1-_w%_c))  // since _r%_c == _c-1
                _(apply ar_read(_data+_w%_c, _c-1-_w%_c);)
                // have ar(_data+_w%_c, _c-1-_w%_c+1) [3]
                //_(dumpstate blah222)
                _(unfold ar_sec(_data+_r%_c+1, _c-_r%_c-1, _l))
            } else {
                // r%c < c-1, then r%c+1 == (r+1)%c

                // already have ar_sec(data, w%c, l) after unfolding rb() [1]
                // already have ar(_data+_w%_c, _r%_c-_w%_c)
                _(apply ar_read(_data+_w%_c, _r%_c-_w%_c);)
                // have ar(_data+_w%_c, _r%_c-_w%_c+1)
                _(apply mod_plusone(_r, _c);)
                // have r%c+1 == (r+1)%c
                _(unfold ar(_data+_w%_c, _r%_c-_w%_c+1))
                _(fold ar(_data+_w%_c, (_r%_c+1)-_w%_c)) //[2]
                // after unfolding already have ar_sec(_data+_r%_c+1, _c-_r%_c-1,_l)
                _(unfold ar_sec(_data+_r%_c+1, _c-_r%_c-1, _l))
                _(fold ar_sec(_data+(_r+1)%_c, _c-(_r+1)%_c, _l)) //[3]
                //_(dumpstate blah333)
            }
        }
    )

    _(fold rb(buf; _data, _c, _w, _r+1, _l))    
    return 0;
  } else {

    /* nothing to read */
    _(fold rb(buf; _data, _c, _w, _r, _l))
    //_(dumpstate blah444)
    return 1;
  }
}

/* clear the buffer efficiently */
void rb_clear(rb_t * buf, int *read_index)
  _(requires exists int * _data, int _c, int _w, int _r, sec _l.
    rb(buf; _data, _c, _w, _r, _l))
  _(requires read_index |-> _r)

  _(ensures rb(buf; _data, _c, _w, _w, low))
  _(ensures read_index |-> _w)
{
  _(unfold rb(buf; _data, _c, _w, _r, _l))
  *read_index = buf->write_index;

  _(apply 
          if (_r%_c <= _w%_c) {
              // already have ar_sec(data+r%c, w%c-r%c, l)
              _(apply arsec2ar(_data+_r%_c, _w%_c-_r%_c, _l);)
              // have ar(data+r%c, w%c-r%c)
              // already have ar(data, r%c)
              _(apply concat_ar(_data, _r%_c, _w%_c-_r%_c);)
              // hence ar(data, r%c+w%c-r%c)  [1]
              _(fold ar_sec(_data+_w%_c, _w%_c-_w%_c, low)) // [2], since len = 0
              // already have ar(data+w%c, c-w%c) [3]
              //_(dumpstate blah111)
          } else {
              // w%c < r%c
              // already have ar_sec(data, w%c, l)
              _(apply arsec2ar(_data, _w%_c, _l);)
              // have ar(data, w%c) [1]
              _(fold ar_sec(_data+_w%_c, _w%_c-_w%_c, low)) // [2], since len = 0             
              // already have ar_sec(data+r%c, c-r%c,l)
              _(apply arsec2ar(_data+_r%_c, _c-_r%_c, _l);)
              // have ar(_data+_r%_c, _c-_r%_c)
              // already have ar(data+w%c, r%c-w%c)
              _(apply concat_ar(_data+_w%_c, _r%_c-_w%_c, _c-_r%_c);)
              // have ar(_data+_w%_c, _r%_c-_w%_c+_c-_r%_c) [3]
              //_(dumpstate blah222)
          }
      )

  _(fold rb(buf; _data, _c, _w, _w, low))
}
